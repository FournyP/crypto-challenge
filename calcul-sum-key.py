import requests
import re
import importlib
controlKey = importlib.import_module("control-key")

def getNumbers():
    response = requests.get("http://univcergy.phpnet.org/scenario3/811193524111958/index.php").text

    return re.findall('[0-9]{9}', response)

def calculSumKey(numbers):
    som = 0
    for number in numbers:
        som = som + controlKey.controlKey(str(number))

    return som


if __name__ == "__main__":
    numbers = getNumbers()
    print(calculSumKey(numbers))
    