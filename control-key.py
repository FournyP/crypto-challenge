import sys

def controlKey(chars):
        factor = [7, 3, 1]
        result = 0

        for index, char in enumerate(chars.upper()):
                if char == '<':
                        char = 0
                elif char >= 'A' and char <= 'Z':
                        char = ord(char) - 55
                else:
                        char = int(char)

                result += char * factor[index % 3]

        return result % 10

if __name__ == "__main__":
    
    if len(sys.argv) != 1:

        key = controlKey(sys.argv[1])
        print(key)

    else:
        raise Exception("Error, please insert 1 parameter")