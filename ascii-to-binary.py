import binascii
import sys

if __name__ == "__main__":

    if len(sys.argv) != 2:
        follow = open(sys.argv[1], 'r')

        chaine = follow.read()

        f = open(sys.argv[2], "x")

        binary = bin(int.from_bytes(chaine.encode(), 'big'))

        f.write(binary)
        f.close()
    else:
        raise Exception("Error, please insert 2 parameters")
